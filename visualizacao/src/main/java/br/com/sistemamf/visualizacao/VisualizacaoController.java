package br.com.sistemamf.visualizacao;

import br.com.sistemamf.visualizacao.security.principal.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VisualizacaoController {
    @PostMapping
    public Usuario getUsuario(@AuthenticationPrincipal Usuario usuario) {
        return usuario;
    }
}
