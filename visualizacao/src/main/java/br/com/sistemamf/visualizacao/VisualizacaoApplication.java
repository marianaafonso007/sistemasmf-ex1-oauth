package br.com.sistemamf.visualizacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VisualizacaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(VisualizacaoApplication.class, args);
	}

}
